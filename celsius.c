#include <stdio.h>

int main(void) {
	
    int tc;
	float tf;
	
	for (tc=-100; tc<=100; tc=tc+2) {
	
		tf=(tc*9./5.)+32;
		printf("%3d = %5.1f\n",tc,tf);
	}
	
	getchar();
	return(0);

}

